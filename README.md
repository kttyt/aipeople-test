* Приложение пакуется в docker образ, который заливается в открытый regestry гитлаба
* Деплоится все ansibl`ом на машину в hetzner, для этого используется моя сборка docker образа с ним
```
FROM python:3-slim
RUN apt-get update \
    && apt-get install -y --no-install-recommends openssh-client \
    && apt-get clean \
    && pip --no-cache-dir install ansible
```
* ssh-ключ используемый для деплоя находится в переменных проекта

Больше всего времени заняла настройка php-fpm (делал это впервые) и запуск ансибла в gitlab ci, тк такого опыта тоже не было
